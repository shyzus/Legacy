module org.shydev.community.client {

    requires transitive javafx.controls;
    requires javafx.fxml;
    requires transitive javafx.graphics;
    requires java.logging;
    requires transitive javafx.base;
    requires java.desktop;
    requires engenius;
    requires NetCom2;

    exports org.shydev.community to javafx.graphics;

    opens org.shydev.community.controller to javafx.fxml;

    opens org.shydev.community.model;
    opens org.shydev.community.service;
    opens org.shydev.community;

    opens scenes;
    opens stylesheets;

}