package org.shydev.community;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.shydev.community.service.PropertiesService;

/**
 * Starting point of the application
 *
 * @author shyzus
 * @version 0.3
 */
public class ClientDriver extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(ClientDriver.class.getResource("/scenes/launcher.fxml"));
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setTitle(PropertiesService.getInstance().getProperty("title"));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
