package org.shydev.community.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import org.shydev.community.service.DialogService;
import org.shydev.community.service.UtilService;

/**
 * Controller for the launcher modal leading to login and registration dialogs.
 *
 * @author shyzus (dev@shyzus.com)
 */
public class LauncherController {

    @FXML
    private BorderPane borderPane;

    @FXML
    private Button loginButton;

    @FXML
    private Button registerButton;


    /**
     * Switches between bordered and borderless launcher.
     * Default is bordered.
     *
     */
    @FXML
    void onBorderToggle() {
        Window window = UtilService.getInstance().getWindowFromNode(borderPane);

        if (window != null) {
            DialogService.getInstance().setOwnerWindow(window);

            if (((Stage) window).getStyle() == StageStyle.DECORATED) {
                UtilService.getInstance().switchScene(borderPane, "launcher", false, ((Stage) window).isMaximized());
            } else if (((Stage) window).getStyle() == StageStyle.UNDECORATED) {
                UtilService.getInstance().switchScene(borderPane, "launcher", true, ((Stage) window).isMaximized());
            }
        }
    }

    /**
     * Open the login dialog
     */
    @FXML
    void onLogin() {
        Window window = UtilService.getInstance().getWindowFromNode(borderPane);

        if (window != null) {
            DialogService.getInstance().setOwnerWindow(window);
            DialogService.getInstance().loginDialog();
        }
    }

    /**
     * Open the registration dialog
     */
    @FXML
    void onRegister() {
        Window window = UtilService.getInstance().getWindowFromNode(borderPane);

        if (window != null) {
            DialogService.getInstance().setOwnerWindow(window);
            DialogService.getInstance().registrationDialog();
        }
    }


}
