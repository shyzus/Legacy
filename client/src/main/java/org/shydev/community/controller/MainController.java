package org.shydev.community.controller;

import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import org.shydev.community.model.Character;
import org.shydev.community.model.Direction;
import org.shydev.community.model.GridMap;
import org.shydev.community.service.ChatService;
import org.shydev.community.service.ConnectionService;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controller responsible for the main screen what the viewer will be using the most to play the game.
 *
 * @author shyzus (dev@shyzus.com)
 */
public class MainController implements Initializable {

    @FXML
    private SplitPane leftSplitPane;

    @FXML
    private StackPane gameWindowContainer;

    @FXML
    private ScrollPane localChatContainer;

    @FXML
    private TextFlow localChatTextFlow;

    @FXML
    private TabPane tabPane;

    @FXML
    private ScrollPane globalChatContainer;

    @FXML
    private TextFlow globalChatTextFlow;

    private double gridMapHeight;
    private double gridMapWidth;

    /**
     * Initialization of the main controller before the scene is loaded on screen this method is executed.
     *
     * @param location  location of scene
     * @param resources resources bundled with scene
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Logger.getLogger(MainController.class.getName()).log(Level.INFO, "Initializing MainController...");

        Character character = ConnectionService.getMyCharacter();

        character.initializeProperties();

        initGameComponents();
        initTabs(character);
        initChat();

        ConnectionService.saveCharacter(character);

        ConnectionService.addCharacter(character);

        Platform.runLater(() -> {

            gameWindowContainer.getScene().setOnKeyPressed(event -> {
                switch (event.getCode()) {
                    case W:
                        if (!character.getDirection().equals(Direction.NORTH) || !character.isMoving()) {
                            ConnectionService.movementStateUpdate(character.getName(), true);
                            ConnectionService.directionUpdate(character.getName(), Direction.NORTH);
                            Logger.getLogger(getClass().getName()).log(Level.INFO, "pressed W");
                        }
                        break;
                    case A:
                        if (!character.getDirection().equals(Direction.WEST) || !character.isMoving()) {
                            ConnectionService.movementStateUpdate(character.getName(), true);
                            ConnectionService.directionUpdate(character.getName(), Direction.WEST);
                            Logger.getLogger(getClass().getName()).log(Level.INFO, "pressed A");
                        }
                        break;
                    case S:
                        if (!character.getDirection().equals(Direction.SOUTH) || !character.isMoving()) {
                            ConnectionService.movementStateUpdate(character.getName(), true);
                            ConnectionService.directionUpdate(character.getName(), Direction.SOUTH);
                            Logger.getLogger(getClass().getName()).log(Level.INFO, "pressed S");
                        }
                        break;
                    case D:
                        if (!character.getDirection().equals(Direction.EAST) || !character.isMoving()) {
                            ConnectionService.movementStateUpdate(character.getName(), true);
                            ConnectionService.directionUpdate(character.getName(), Direction.EAST);
                            Logger.getLogger(getClass().getName()).log(Level.INFO, "pressed D");
                        }
                        break;
                    default:
                        Logger.getLogger(getClass().getName()).log(Level.INFO, "Pressed unmapped key.");
                        break;
                }
            });

            gameWindowContainer.getScene().setOnKeyReleased(event -> {
                System.out.println(character.getDirection());
                switch (event.getCode()) {
                    case W:
                        if (character.getDirection().equals(Direction.NORTH)) {
                            ConnectionService.movementStateUpdate(character.getName(), false);
                        }
                        break;
                    case A:
                        if (character.getDirection().equals(Direction.WEST)) {
                            ConnectionService.movementStateUpdate(character.getName(), false);
                        }
                        break;
                    case S:
                        if (character.getDirection().equals(Direction.SOUTH)) {
                            ConnectionService.movementStateUpdate(character.getName(), false);
                        }
                        break;
                    case D:
                        if (character.getDirection().equals(Direction.EAST)) {
                            ConnectionService.movementStateUpdate(character.getName(), false);
                        }
                        break;
                    default:
                        Logger.getLogger(getClass().getName()).log(Level.INFO, "Released unmapped key.");
                        break;
                }

                initExit();

            });
        });

    }

    /**
     * Initialize game components such as the map
     */
    private void initGameComponents() {

        gameWindowContainer.setAlignment(Pos.TOP_LEFT);
        leftSplitPane.setMaxWidth(900);
        leftSplitPane.setMinWidth(900);

        gameWindowContainer.setMaxHeight(605);
        gameWindowContainer.setMinHeight(605);

        Image image = new Image(getClass().getResourceAsStream("/assets/tiles/grass.png"));
        GridMap gridMap = new GridMap(200, 200, image);

        gridMapHeight = gridMap.getHeight();
        gridMapWidth = gridMap.getWidth();

        gameWindowContainer.getChildren().addAll(gridMap);

    }

    /**
     * Initialize the chat boxes and listeners
     */
    private void initChat() {

        Text welcomeText = new Text("Welcome to Legacy, a tribute to Generations.\n");
        welcomeText.setFill(Color.WHITE);

        globalChatTextFlow.getChildren().add(welcomeText);

        localChatTextFlow.getChildren().addListener((ListChangeListener<? super Node>) listChangelistener -> {
            listChangelistener.next();
            if (listChangelistener.wasAdded()) {
                localChatContainer.setVvalue(localChatContainer.getVmax());
            }
        });

        globalChatTextFlow.getChildren().addListener((ListChangeListener<? super Node>) listChangelistener -> {
            listChangelistener.next();
            if (listChangelistener.wasAdded()) {
                globalChatContainer.setVvalue(globalChatContainer.getVmax());
            }
        });

    }

    /**
     * Initialize the tabs containing information about the character
     *
     * @param character character
     */
    private void initTabs(Character character) {

        Tab stats = new Tab("Stats");

        HBox statsHBox = new HBox(10);

        Label name = new Label("Name: " + character.getName());
        statsHBox.getChildren().addAll(name);

        stats.setContent(statsHBox);

        Tab inventory = new Tab("Inventory");
        Tab options = new Tab("Options");

        HBox optionsHBox = new HBox(10);

        Label say = new Label("Say");
        say.setOnMouseClicked(event -> ChatService.getInstance().icChat(localChatTextFlow, character.getName()));

        Label ooc = new Label("OOC");
        ooc.setOnMouseClicked(event -> ChatService.getInstance().oocChat(globalChatTextFlow, character.getName()));

        optionsHBox.getChildren().addAll(say, ooc);

        options.setContent(optionsHBox);

        Tab skills = new Tab("Skills");

        tabPane.getTabs().addAll(stats, inventory, options, skills);
    }

    private void initExit() {
        gameWindowContainer.getScene().getWindow().setOnCloseRequest(windowEvent -> ConnectionService.exit());
    }

    /**
     * Adds a node to the screen(stackPane) always comes on top
     *
     * @param node node to put on screen
     */
    public void addNode(Node node) {
        gameWindowContainer.getChildren().add(node);
    }

    public void removeNode(Node node) {
        gameWindowContainer.getChildren().remove(node);
    }

    public StackPane getGameWindowContainer() {
        return gameWindowContainer;
    }

    public double getGridMapHeight() {
        return gridMapHeight;
    }

    public double getGridMapWidth() {
        return gridMapWidth;
    }
}
