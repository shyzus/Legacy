package org.shydev.community.model;

import javafx.scene.image.Image;
import org.shydev.community.service.ConnectionService;
import org.shydev.community.service.UtilService;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Animation handler manages changes to animation state of the character
 *
 * @author shyzus (dev@shyzus.com)
 */
public class AnimationHandler implements Runnable {

    private Character character;
    private HashMap<String, List<Image>> loadedImages = new HashMap<>();

    public AnimationHandler(Character character) {
        super();
        this.character = character;
    }

    @Override
    public void run() {

        while(ConnectionService.isRunning()) {

            StringBuilder stringBuilder = new StringBuilder(ResourcePath.SPRITE_BASE.getValue());

            stringBuilder.append("/").append(character.getSkin());

            switch (character.getDirection()) {
                case NORTH:
                    stringBuilder.append("/").append(Direction.NORTH.getValue());
                    break;
                case WEST:
                    stringBuilder.append("/").append(Direction.WEST.getValue());
                    break;
                case SOUTH:
                    stringBuilder.append("/").append(Direction.SOUTH.getValue());
                    break;
                case EAST:
                    stringBuilder.append("/").append(Direction.EAST.getValue());
                    break;
            }

            if (character.isMoving()) {
                stringBuilder.append("/").append("walking");
            } else {
                stringBuilder.append("/").append("standing");
            }

            Path path = Paths.get(stringBuilder.toString());
            String targetUrl = path.toString().replace("\\", "/");

            List<Image> images = new ArrayList<>();

            if (loadedImages.get(targetUrl) == null) {
                List<String> urls = UtilService.getInstance().getDirectoryContents(targetUrl);
                for (String url: urls) {
                    url = url.replace("\\", "/");
                    images.add(new Image(getClass().getResourceAsStream(url)));
                }

                loadedImages.put(targetUrl, images);
            } else {
                images = loadedImages.get(targetUrl);
            }

            // TODO: fix magic number make it dynamic based on images
            int delay = 200 / images.size();

            images.forEach(image -> {
                if (!targetUrl.equals(character.getUrl().split("/")[6])) {
                    character.setImage(image);
                }

                try {
                    // Maintain 60FPS
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    Logger.getLogger(AnimationHandler.class.getName()).log(Level.SEVERE, e.getMessage());
                    Thread.currentThread().interrupt();
                }
            });


        }

    }
}
