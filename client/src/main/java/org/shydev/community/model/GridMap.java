package org.shydev.community.model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;

public class GridMap extends Region {

    private volatile ImageView[][] tiles;

    public GridMap(int horizontalTileCount, int verticalTileCount) {
        this.tiles = new ImageView[horizontalTileCount][verticalTileCount];
    }

    public GridMap(int horizontalTileCount, int verticalTileCount, Image baseTile) {

        this.tiles = new ImageView[horizontalTileCount][verticalTileCount];
        setWidth(horizontalTileCount * 32.00);
        setHeight(verticalTileCount * 32.00);

        for (int j = 0; j < verticalTileCount; j++) {

            for (int i = 0; i < horizontalTileCount; i++) {
                tiles[i][j] = new ImageView();
                tiles[i][j].setImage(baseTile);
                tiles[i][j].setX(i * baseTile.getWidth());
                tiles[i][j].setY(j * baseTile.getHeight());
                getChildren().add(tiles[i][j]);
            }

        }
    }


}
