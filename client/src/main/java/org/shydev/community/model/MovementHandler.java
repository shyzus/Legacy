package org.shydev.community.model;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;
import org.shydev.community.service.ConnectionService;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TODO: remove magic numbers in run method
 * Movement handler is responsible for handling all inputs that affect movement and perform the calculations
 *
 * @author shyzus (dev@shyzus.com)
 */
public class MovementHandler implements Runnable {

    private Character character;
    private double gridMapX;
    private double gridMapY;
    private StackPane gameWindowContainer;

    public MovementHandler(Character character, double gridMapX, double gridMapY, StackPane gameWindowContainer) {
        this.character = character;
        this.gridMapX = gridMapX;
        this.gridMapY = gridMapY;
        this.gameWindowContainer = gameWindowContainer;
    }

    @Override
    public void run() {

        while (ConnectionService.isRunning()) {
            if (character.isMoving()) {

                switch (character.getDirection()) {
                    case NORTH:
                        if (boundsCheck(Direction.NORTH, character)) {
                            while (checkCollision(character)) {
                                Platform.runLater(() -> character.setTranslateY(character.getTranslateY() + 5));
                            }

                            Platform.runLater(() -> character.setTranslateY(character.getTranslateY() - 5));
                            if (character.getTranslateY() < gridMapY - 268 && character.getTranslateY() > 268
                                    && character.getName().equals(ConnectionService.getMyCharacter().getName())) {
                                Platform.runLater(() -> gameWindowContainer.setTranslateY(gameWindowContainer.getTranslateY() + 5));
                            }

                        }

                        break;
                    case WEST:
                        if (boundsCheck(Direction.WEST, character)) {
                            while (checkCollision(character)) {
                                Platform.runLater(() -> character.setTranslateX(character.getTranslateX() + 5));
                            }

                            Platform.runLater(() -> character.setTranslateX(character.getTranslateX() - 5));
                            if (character.getTranslateX() > 418 && character.getTranslateX() < gridMapX - 418
                                    && character.getName().equals(ConnectionService.getMyCharacter().getName())) {
                                Platform.runLater(() -> gameWindowContainer.setTranslateX(gameWindowContainer.getTranslateX() + 5));

                            }
                        }

                        break;
                    case SOUTH:
                        if (boundsCheck(Direction.SOUTH, character)) {
                            while (checkCollision(character)) {
                                Platform.runLater(() -> character.setTranslateY(character.getTranslateY() - 5));
                            }

                            Platform.runLater(() -> character.setTranslateY(character.getTranslateY() + 5));
                            if (character.getTranslateY() > 268 && character.getTranslateY() < gridMapY - 268
                                    && character.getName().equals(ConnectionService.getMyCharacter().getName())) {
                                Platform.runLater(() -> gameWindowContainer.setTranslateY(gameWindowContainer.getTranslateY() - 5));

                            }

                        }

                        break;
                    case EAST:
                        if (boundsCheck(Direction.EAST, character)) {
                            while (checkCollision(character)) {
                                Platform.runLater(() -> character.setTranslateX(character.getTranslateX() - 5));
                            }

                            Platform.runLater(() -> character.setTranslateX(character.getTranslateX() + 5));
                            if (character.getTranslateX() < gridMapX - 418 && character.getTranslateX() > 418
                                    && character.getName().equals(ConnectionService.getMyCharacter().getName())) {
                                Platform.runLater(() -> gameWindowContainer.setTranslateX(gameWindowContainer.getTranslateX() - 5));

                            }

                        }

                        break;

                }

            }


            // Wait 1/60 of a second
            try {
                Thread.sleep(16);
            } catch (InterruptedException e) {
                Logger.getLogger(MovementHandler.class.getName()).log(Level.SEVERE, e.getMessage());
                Thread.currentThread().interrupt();
            }
        }

    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }

    private boolean checkCollision(Node block) {
        for (Node static_bloc : gameWindowContainer.getChildren()) {
            if (static_bloc != block
                    && block.getBoundsInParent().intersects(static_bloc.getBoundsInParent())
                    && static_bloc instanceof Character) {

                return true;
            }
        }

        return false;
    }

    private boolean boundsCheck(Direction direction, Character character) {

        switch (direction) {
            case NORTH:
                return character.getTranslateY() - 5 > 0;
            case WEST:
                return character.getTranslateX() - 5 > 0;
            case SOUTH:
                return character.getTranslateY() + 5 < gridMapY - 32;
            case EAST:
                return character.getTranslateX() + 5 < gridMapX - 32;
        }

        return false;
    }

}
