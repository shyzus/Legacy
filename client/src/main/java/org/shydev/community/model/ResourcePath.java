package org.shydev.community.model;

public enum ResourcePath {
    ROOT("/assets"), TILE(ROOT.getValue().concat("/tiles")), SPRITE_BASE(ROOT.getValue().concat("/base"));

    private String value;

    ResourcePath(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
