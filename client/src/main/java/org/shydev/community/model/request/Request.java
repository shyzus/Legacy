package org.shydev.community.model.request;

import java.io.Serializable;
import java.util.EnumMap;

public class Request implements Serializable {

    private RequestType requestType;
    private EnumMap<RequestVariable, Object> contentMap;

    public Request(RequestType requestType, EnumMap<RequestVariable, Object> contentMap) {
        this.requestType = requestType;
        this.contentMap = contentMap;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public EnumMap<RequestVariable, Object> getContentMap() {
        return contentMap;
    }

    public void setContentMap(EnumMap<RequestVariable, Object> contentMap) {
        this.contentMap = contentMap;
    }
}
