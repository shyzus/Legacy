package org.shydev.community.service;

import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class ChatService {

    private static ChatService ourInstance = new ChatService();

    public static ChatService getInstance() {
        return ourInstance;
    }

    private ChatService() {

    }

    public void icChat(TextFlow textFlow, String name) {
        String content = name + " - \"" + DialogService.getInstance().icDialog() + "\"\n";
        Text text = new Text(content);
        text.setFill(Color.WHITE);
        textFlow.getChildren().add(text);
    }

    public void oocChat(TextFlow textFlow, String name) {
        String content = name + " - \"" + DialogService.getInstance().oocDialog() + "\"\n";
        Text text = new Text(content);
        text.setFill(Color.WHITE);
        textFlow.getChildren().add(text);
    }
}
