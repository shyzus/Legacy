package org.shydev.community.service;

import com.github.thorbenkuck.netcom2.exceptions.StartFailedException;
import com.github.thorbenkuck.netcom2.network.shared.CommunicationRegistration;
import nl.hva.fdmci.tsse.engine.service.network.NetworkService;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;
import javafx.util.Pair;
import org.shydev.community.controller.MainController;
import org.shydev.community.model.AnimationHandler;
import org.shydev.community.model.Character;
import org.shydev.community.model.Direction;
import org.shydev.community.model.MovementHandler;
import org.shydev.community.model.request.Request;
import org.shydev.community.model.request.RequestType;
import org.shydev.community.model.request.RequestVariable;

import java.util.EnumMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ForkJoinPool;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionService {

    private static final NetworkService NETWORK_INSTANCE = new NetworkService();
    private static final ObservableMap<String, Character> PLAYER_MAP = FXCollections.observableMap(new ConcurrentHashMap<>());
    private static final EnumMap<RequestVariable, Object> CONTENT_MAP = new EnumMap<>(RequestVariable.class);
    private static Character myCharacter;

    private static void init() {

        try {
            NETWORK_INSTANCE.launchClient(PropertiesService.getInstance().getProperty("ip"),
                    Integer.parseInt(PropertiesService.getInstance().getProperty("port")));
        } catch (StartFailedException e) {
            DialogService.getInstance().errorModal("Unable to connect to Legacy Server!");
            System.exit(0);
        }

        register(NETWORK_INSTANCE.getClientCommunicationRegistration());

        PLAYER_MAP.addListener((MapChangeListener<String, Character>) change -> {

            if (change.wasAdded()) {

                var ref = new Object() {
                    AnimationHandler animationHandler;
                    MovementHandler movementHandler;
                };

                Platform.runLater(() -> {
                    MainController mainController = ((MainController) UtilService.getInstance().getActiveController());

                    mainController.addNode(change.getValueAdded());

                    ref.animationHandler = new AnimationHandler(change.getValueAdded());

                    ref.movementHandler = new MovementHandler(change.getValueAdded(),
                            mainController.getGridMapWidth(), mainController.getGridMapHeight(),
                            mainController.getGameWindowContainer());

                    ForkJoinPool.commonPool().execute(ref.animationHandler);
                    ForkJoinPool.commonPool().execute(ref.movementHandler);
                });

                change.getValueAdded().initializeProperties();

                change.getValueAdded().getMovingProperty().addListener((observable, oldValue, newValue) ->
                        ConnectionService.movementStateUpdate(change.getKey(), newValue));

                change.getValueAdded().getDirectionProperty().addListener((observable, oldValue, newValue) ->
                        ConnectionService.directionUpdate(change.getKey(), newValue));
            } else if (change.wasRemoved()) {

                Platform.runLater(() -> {
                    MainController mainController = ((MainController) UtilService.getInstance().getActiveController());
                    mainController.removeNode(change.getValueRemoved());
                });

            }

        });

    }

    public static void exit() {
        CONTENT_MAP.put(RequestVariable.USERNAME, myCharacter.getName());
        NETWORK_INSTANCE.getClientSender().objectToServer(new Request(RequestType.LOGOUT, CONTENT_MAP));
        CONTENT_MAP.clear();
        Logger.getLogger(ConnectionService.class.getName()).log(Level.INFO, "Logged out user! {0}", myCharacter.getName());
    }

    public static void movementStateUpdate(String username, boolean isMoving) {
        CONTENT_MAP.put(RequestVariable.USERNAME, username);
        CONTENT_MAP.put(RequestVariable.IS_MOVING, isMoving);
        NETWORK_INSTANCE.getClientSender().objectToServer(new Request(RequestType.MOVE, CONTENT_MAP));
        CONTENT_MAP.clear();
        Logger.getLogger(ConnectionService.class.getName()).log(Level.INFO, "Send movement update! {0}", isMoving);
    }

    public static void directionUpdate(String username, Direction direction) {
        CONTENT_MAP.put(RequestVariable.USERNAME, username);
        CONTENT_MAP.put(RequestVariable.DIRECTION, direction);
        NETWORK_INSTANCE.getClientSender().objectToServer(new Request(RequestType.DIRECTION, CONTENT_MAP));
        CONTENT_MAP.clear();
        Logger.getLogger(ConnectionService.class.getName()).log(Level.INFO, "Send direction update! {0}", direction.getValue());
    }

    public static void addCharacter(Character character) {
        CONTENT_MAP.put(RequestVariable.CHARACTER, character);
        NETWORK_INSTANCE.getClientSender().objectToServer(new Request(RequestType.ADD_CHARACTER, CONTENT_MAP));
        Logger.getLogger(ConnectionService.class.getName()).log(Level.INFO, "Send add character request: {0}", character.getName());
    }

    static void login(Pair<String, String> loginDetails) {
        if (!NETWORK_INSTANCE.isClientRunning()) {
            init();
        }
        CONTENT_MAP.put(RequestVariable.USERNAME, loginDetails.getKey());
        CONTENT_MAP.put(RequestVariable.PASSWORD, loginDetails.getValue());
        NETWORK_INSTANCE.getClientSender().objectToServer(new Request(RequestType.LOGIN, CONTENT_MAP));
        CONTENT_MAP.clear();
    }

    static void register(String[] registrationDetails) {
        if (!NETWORK_INSTANCE.isClientRunning()) {
            init();
        }

        CONTENT_MAP.put(RequestVariable.USERNAME, registrationDetails[0]);
        CONTENT_MAP.put(RequestVariable.PASSWORD, registrationDetails[1]);
        CONTENT_MAP.put(RequestVariable.EMAIL, registrationDetails[2]);
        NETWORK_INSTANCE.getClientSender().objectToServer(new Request(RequestType.REGISTRATION, CONTENT_MAP));
        CONTENT_MAP.clear();
    }

    public static void saveCharacter(Character character) {
        CONTENT_MAP.put(RequestVariable.CHARACTER, character);
        NETWORK_INSTANCE.getClientSender().objectToServer(new Request(RequestType.SAVE_CHARACTER, CONTENT_MAP));
        CONTENT_MAP.clear();
    }

    public static boolean isRunning() {
        return NETWORK_INSTANCE.isClientRunning();
    }

    private static void register(CommunicationRegistration communicationRegistration) {
        communicationRegistration.register(Request.class).addLast((connectionContext, session, request) -> {
            switch (request.getRequestType()) {
                case MOVE:
                    Character modified = PLAYER_MAP.get(request.getContentMap().get(RequestVariable.USERNAME));
                    modified.setMoving((boolean) request.getContentMap().get(RequestVariable.IS_MOVING));
                    PLAYER_MAP.replace(modified.getName(), modified);
                    if (myCharacter.getName().equals(modified.getName())) {
                        myCharacter.setMoving(modified.isMoving());
                    }
                    Logger.getLogger(ConnectionService.class.getName()).log(Level.INFO, "Received move request");
                    break;
                case SYNC:
                    Logger.getLogger(ConnectionService.class.getName()).log(Level.INFO, "Received sync request");

                    ((ConcurrentMap<String, Character>) request.getContentMap().get(RequestVariable.PLAYER_MAP)).forEach((username, character) -> {
                        character.initializeProperties();
                        PLAYER_MAP.putIfAbsent(username, character);
                    });

                    Logger.getLogger(ConnectionService.class.getName()).log(Level.INFO, "Finished processing sync request");
                    break;
                case DIRECTION:
                    Character modifiedPlayer = PLAYER_MAP.get(request.getContentMap().get(RequestVariable.USERNAME));
                    System.out.println("FROM SERVER:" + (Direction) request.getContentMap().get(RequestVariable.DIRECTION));
                    modifiedPlayer.setDirection((Direction) request.getContentMap().get(RequestVariable.DIRECTION));
                    PLAYER_MAP.replace(modifiedPlayer.getName(), modifiedPlayer);
                    if (myCharacter.getName().equals(modifiedPlayer.getName())) {
                        myCharacter.setDirection(modifiedPlayer.getDirection());
                    }
                    Logger.getLogger(ConnectionService.class.getName()).log(Level.INFO, "Received direction request");
                    break;
                case ADD_CHARACTER:
                    PLAYER_MAP.putIfAbsent(((Character) request.getContentMap().get(RequestVariable.CHARACTER)).getName(), (Character) request.getContentMap().get(RequestVariable.CHARACTER));
                    Logger.getLogger(ConnectionService.class.getName()).log(Level.INFO, "Received add request");
                    break;
                case LOGIN:
                    if (!(boolean) request.getContentMap().get(RequestVariable.LOGIN_CONFIRM)) {
                        Logger.getLogger(ConnectionService.class.getName()).log(Level.INFO, "Login refused!");
                        Platform.runLater(() -> DialogService.getInstance().errorModal("Login refused: Invalid username/password."));
                    } else {
                        Logger.getLogger(ConnectionService.class.getName()).log(Level.INFO, "Login accepted!");
                        if (request.getContentMap().get(RequestVariable.CHARACTER) != null) {
                            Platform.runLater(() -> {
                                myCharacter = (Character) request.getContentMap().get(RequestVariable.CHARACTER);
                                UtilService.getInstance().switchScene(null, "main", true, true);
                                PLAYER_MAP.putIfAbsent(myCharacter.getName(), myCharacter);
                            });
                        } else {
                            Platform.runLater(() -> {
                                myCharacter = new Character("/assets/base/white/south/standing/0.png",
                                        (String) request.getContentMap().get(RequestVariable.USERNAME), null);
                                UtilService.getInstance().switchScene(null, "main", true, true);
                            });
                        }
                    }
                    break;
                case REGISTRATION:
                    if (!(boolean) request.getContentMap().get(RequestVariable.REGISTRATION_CONFIRM)) {
                        Logger.getLogger(ConnectionService.class.getName()).log(Level.INFO, "Registration refused!");
                        Platform.runLater(() -> DialogService.getInstance().errorModal("Registration refused: Invalid username/password/email."));
                    } else {
                        Logger.getLogger(ConnectionService.class.getName()).log(Level.INFO, "Registration accepted!");
                        Platform.runLater(() -> UtilService.getInstance().switchScene(null, "main", true, true));
                    }
                    break;
                default:
                    Logger.getLogger(ConnectionService.class.getName()).log(Level.WARNING, "Unsupported request type received: {0}", request.getRequestType());
                    break;

            }
        });
    }

    public static Character getMyCharacter() {
        return myCharacter;
    }

}
