package org.shydev.community.service;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.stage.Window;
import javafx.util.Pair;

import java.security.NoSuchAlgorithmException;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Dialog service is responsible for displaying dialogs of any kind
 *
 * @author shyzus (dev@shyzus.com)
 */
public class DialogService {

    private static DialogService ourInstance = new DialogService();
    private Window ownerWindow;

    private DialogService() {
    }

    public static DialogService getInstance() {
        return ourInstance;
    }

    Window getOwnerWindow() {
        return ownerWindow;
    }

    public void setOwnerWindow(Window ownerWindow) {
        this.ownerWindow = ownerWindow;
    }

    void errorModal(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("Error");
        alert.setContentText(message);
        alert.setTitle("Error");

        alert.showAndWait();

    }

    public String confirmStopServer() {

        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Stop Server");
        dialog.setHeaderText("Please submit a reason to stop the server.");
        dialog.initOwner(ownerWindow);

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            if (UtilService.getInstance().isNullOrEmpty(result.get())) {
                Alert invalid = new Alert(Alert.AlertType.WARNING);
                invalid.setTitle("Not Allowed");
                invalid.setHeaderText("Invalid Input!");
                invalid.setContentText("You cannot stop the server without supplying a reason!");
                invalid.showAndWait();
                return null;
            }

            return result.get();
        } else {
            return null;
        }

    }

    public String confirmRestartServer() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Restart Server");
        dialog.setHeaderText("Please submit a reason to restart the server.");
        dialog.initOwner(ownerWindow);

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            if (UtilService.getInstance().isNullOrEmpty(result.get())) {
                Alert invalid = new Alert(Alert.AlertType.WARNING);
                invalid.setTitle("Not Allowed");
                invalid.setHeaderText("Invalid Input!");
                invalid.setContentText("You cannot restart the server without supplying a reason!");
                invalid.showAndWait();
                return null;
            }

            return result.get();
        } else {
            return null;
        }
    }

    /**
     * New text field window for IC dialog
     *
     * @return submitted text
     */
    String icDialog() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("IC dialog");
        dialog.setGraphic(null);
        dialog.setHeaderText(null);
        dialog.setContentText(null);
        dialog.initOwner(ownerWindow);

        Optional<String> result = dialog.showAndWait();

        return result.orElse(null);
    }

    public String roleplayDialog() {
        Dialog<String> dialog = new Dialog<>();
        dialog.setTitle("Roleplay Dialog");
        dialog.setHeaderText(null);
        dialog.setContentText(null);
        dialog.setGraphic(null);
        dialog.initOwner(ownerWindow);

        ButtonType okButtonType = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(okButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);

        TextArea textArea = new TextArea();
        textArea.setWrapText(true);

        grid.add(textArea, 0, 0);

        Node loginButton = dialog.getDialogPane().lookupButton(okButtonType);
        loginButton.setDisable(true);

        dialog.getDialogPane().setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                Platform.runLater(loginButton::requestFocus);
            }

        });

        textArea.textProperty().addListener((observable, oldValue, newValue) -> loginButton.setDisable(newValue.trim().isEmpty()));

        dialog.getDialogPane().setContent(grid);

        Platform.runLater(textArea::requestFocus);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == okButtonType) {
                return textArea.getText();
            }
            return null;
        });

        Optional<String> result = dialog.showAndWait();

        return result.orElse(null);
    }

    String oocDialog() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("OOC dialog");
        dialog.setGraphic(null);
        dialog.setHeaderText(null);
        dialog.setContentText(null);
        dialog.initOwner(ownerWindow);

        Optional<String> result = dialog.showAndWait();

        return result.orElse(null);
    }

    public double setSpeedDialog() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Set Speed");
        dialog.setGraphic(null);
        dialog.setHeaderText("Set a speed");
        dialog.setContentText(null);
        dialog.initOwner(ownerWindow);

        Optional<String> result = dialog.showAndWait();

        try {
            if (result.isPresent()) {
                return Double.parseDouble(result.get());
            }

        } catch (NoSuchElementException | NumberFormatException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Incorrect Input");
            alert.setContentText("The submitted value was rejected.");
            alert.showAndWait();
        }

        return 0;
    }

    public void loginDialog() {
        // Create the custom dialog.
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Login Dialog");
        dialog.setHeaderText("Please fill in the fields below.");

        // Set the button types.
        ButtonType loginButtonType = new ButtonType("Login", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        // Create the username and password labels and fields.
        GridPane grid = new GridPane();
        grid.setId("dialogGrid");
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField username = new TextField();
        username.setPromptText("Username");
        username.setId("usernameField");
        PasswordField password = new PasswordField();
        password.setPromptText("Password");
        password.setId("passwordField");

        grid.add(new Label("Username:"), 0, 0);
        grid.add(username, 1, 0);
        grid.add(new Label("Password:"), 0, 1);
        grid.add(password, 1, 1);

        // Enable/Disable login button depending on whether a username was entered.
        Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        loginButton.setDisable(true);
        loginButton.setId("loginButton");

        // Do some validation (using the Java 8 lambda syntax).
        ChangeListener<String> changeListener = (observable, newValue, oldValue) ->
                loginButton.setDisable(UtilService.getInstance().isNullOrEmpty(username.getText().trim(), password.getText().trim()));
        username.textProperty().addListener(changeListener);
        password.textProperty().addListener(changeListener);

        dialog.getDialogPane().setContent(grid);

        // Request focus on the username field by default.
        Platform.runLater(username::requestFocus);

        // Convert the result to a username-password-pair when the login button is clicked.
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                try {
                    return new Pair<>(username.getText(), SecurityService.getInstance().hash(password.getText(), "SHA-256"));
                } catch (NoSuchAlgorithmException e) {
                    Logger.getLogger(DialogService.class.getName()).log(Level.SEVERE, e.getMessage());
                }
            }
            return null;
        });

        Optional<Pair<String, String>> loginResult = dialog.showAndWait();

        loginResult.ifPresent(ConnectionService::login);
    }

    public void registrationDialog() {
        Dialog<String[]> dialog = new Dialog<>();
        dialog.setTitle("Registration Dialog");
        dialog.setHeaderText("Fill in your details below");

        // Set the button types.
        ButtonType loginButtonType = new ButtonType("Register", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        // Create the username and password labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField username = new TextField();
        username.setPromptText("Username");
        PasswordField password = new PasswordField();
        password.setPromptText("Password");
        TextField email = new TextField();
        email.setPromptText("Email");

        grid.add(new Label("Username:"), 0, 0);
        grid.add(username, 1, 0);
        grid.add(new Label("Password:"), 0, 1);
        grid.add(password, 1, 1);
        grid.add(new Label("Email:"), 0, 2);
        grid.add(email, 1, 2);

        // Enable/Disable login button depending on whether a username was entered.
        Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        loginButton.setDisable(true);

        // Do some validation (using the Java 8 lambda syntax).
        ChangeListener<String> changeListener = (observable, oldValue, newValue) -> {

            if (email.getText().contains("@") && email.getText().contains(".") && !email.getText().startsWith(".")
                    && !email.getText().endsWith(".") && !email.getText().endsWith("@")
                    && !email.getText().startsWith("@")) {

                loginButton.setDisable(UtilService.getInstance().isNullOrEmpty(username.getText().trim()
                        , password.getText().trim(), email.getText().trim()));
            }
        };

        username.textProperty().addListener(changeListener);
        password.textProperty().addListener(changeListener);
        email.textProperty().addListener(changeListener);

        dialog.getDialogPane().setContent(grid);

        // Request focus on the username field by default.
        Platform.runLater(username::requestFocus);

        // Convert the result to a username-password-pair when the login button is clicked.
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                try {
                    return new String[]{username.getText(), SecurityService.getInstance().hash(password.getText(), "SHA-256"), email.getText()};
                } catch (NoSuchAlgorithmException e) {
                    Logger.getLogger(DialogService.class.getName()).log(Level.SEVERE, e.getMessage());
                }
            }
            return null;
        });

        Optional<String[]> registrationResult = dialog.showAndWait();

        registrationResult.ifPresent(ConnectionService::register);
    }
}
