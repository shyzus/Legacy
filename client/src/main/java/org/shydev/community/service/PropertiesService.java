package org.shydev.community.service;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Properties service responsible for CRUD operations on the legacy properties file
 *
 * @author shyzus (dev@shyzus.com)
 */
public class PropertiesService {

    private static final String CONFIG_FILE_LOCATION = "/config.properties";

    private static PropertiesService ourInstance = new PropertiesService();

    public static PropertiesService getInstance() {
        return ourInstance;
    }

    private PropertiesService() {
    }

    /**
     * Fetch all properties in a Properties object
     *
     * @return Legacy Properties
     * @see Properties
     */
    private Properties getProperties() {
        Properties properties;

        try (InputStream input = UtilService.class.getResourceAsStream(CONFIG_FILE_LOCATION)) {

            // load a properties file
            properties = new Properties();
            properties.load(input);

        } catch (IOException e) {
            properties = null;
            Logger.getLogger(PropertiesService.class.getName()).log(Level.WARNING, e.getMessage());
        }

        return properties;
    }

    void setProperties(Properties newProperties) {

        try (OutputStream output = new FileOutputStream(new File(UtilService.class.getResource(CONFIG_FILE_LOCATION).getPath()))) {

            newProperties.store(output, "Saved last on: " + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

        } catch (IOException e) {
            Logger.getLogger(PropertiesService.class.getName()).log(Level.SEVERE, e.getMessage());
        }

    }

    /**
     * Returns all properties in a string array
     *
     * @param propertyNames property names to match
     * @return Array of properties
     */
    String[] getProperties(String... propertyNames) {
        String[] properties = new String[propertyNames.length];

        for (int i = 0; i < propertyNames.length; i++) {
            properties[i] = getProperty(propertyNames[i]);
        }

        return properties;
    }

    public String getProperty(String propertyName) {

        String property = null;

        try {
            Properties properties = getProperties();

            if (properties == null) {
                throw new NullPointerException();
            }

            property = properties.getProperty(propertyName);
        } catch (NullPointerException e) {
            Logger.getLogger(PropertiesService.class.getName()).log(Level.WARNING, e.getMessage());
        }

        return property;
    }

    public void setProperty(String name, String value) {

        Properties properties = getProperties();

        if (properties == null) {
            properties = new Properties();
        }

        try (OutputStream output = new FileOutputStream(new File(UtilService.class.getResource(CONFIG_FILE_LOCATION).getPath()))) {
            properties.setProperty(name, value);
            properties.store(output, "Saved last on: " + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

        } catch (IOException e) {
            Logger.getLogger(PropertiesService.class.getName()).log(Level.WARNING, e.getMessage());
        }
    }
}
