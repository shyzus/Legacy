package org.shydev.community.service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Security service responsible for securing data
 *
 * @author shyzus (dev@shyzus.com)
 */
public class SecurityService {

    private static SecurityService ourInstance = new SecurityService();

    public static SecurityService getInstance() {
        return ourInstance;
    }

    private SecurityService() {
    }

    /**
     * Hash passwords
     *
     * @param password password
     * @param algorithm algorithm
     * @return Hashed password
     * @throws NoSuchAlgorithmException if algorithm is not found
     * @throws IllegalArgumentException if password is empty
     */
    String hash(String password, String algorithm) throws NoSuchAlgorithmException {
        if (password == null || password.isEmpty()) {
            throw new IllegalArgumentException("Password cannot be empty.");
        } else {
            MessageDigest messageDigest;
            messageDigest = MessageDigest.getInstance(algorithm);
            byte[] sha256sum;
            sha256sum = messageDigest.digest(password.getBytes());
            return String.format("%032X", new BigInteger(1, sha256sum));
        }
    }
}
