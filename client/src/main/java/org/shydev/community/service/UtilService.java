package org.shydev.community.service;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * Utility service for helping with basic IO and managing global variables and methods.
 *
 * @author shyzus (dev@shyzus.com)
 */
public class UtilService {

    private static UtilService ourInstance = new UtilService();
    private Object activeController;
    private FileSystem fileSystem = null;

    public static UtilService getInstance() {
        return ourInstance;
    }

    private UtilService() {
    }

    /**
     * Check whether a string or array of strings are null or empty
     *
     * @param targets String or array of strings
     * @return true if null or empty, false if defined
     */
    boolean isNullOrEmpty(String... targets) {

        for (String target : targets) {
            if (target == null || target.isEmpty() || target.trim().length() == 0) {
                return true;
            }
        }

        return false;
    }

    public Window getWindowFromNode(Node node) {
        return node.getScene().getWindow();
    }

    /**
     * Switches to the specificed scene
     *
     * @param node      A currentWindowNode that is rendered inside of a controller
     * @param sceneName Name of the scene to be switched to
     * @param border    Wether to display a decorated window or undecorated
     * @param maximized Wether to display maximized or not
     */
    public void switchScene(Node node, String sceneName, boolean border, boolean maximized) {

        Platform.runLater(() -> {

            Stage oldStage;

            if (node == null) {
                oldStage = (Stage) DialogService.getInstance().getOwnerWindow();
            } else {
                oldStage = (Stage) node.getScene().getWindow();
            }

            Stage newStage = new Stage();

            // TODO: add logout event on closing window

            Parent root = null;

            try {
                FXMLLoader fxmlLoader = new FXMLLoader(UtilService.class.getResource("/scenes/" + sceneName + ".fxml"));
                root = fxmlLoader.load();
                activeController = fxmlLoader.getController();
            } catch (IOException e) {
                Logger.getLogger(UtilService.class.getName()).log(Level.WARNING, e.getMessage());
            }

            if (root != null) {
                Scene scene = new Scene(root);
                newStage.setScene(scene);
                oldStage.hide();
                if (border) {
                    newStage.initStyle(StageStyle.DECORATED);
                } else {
                    newStage.initStyle(StageStyle.UNDECORATED);
                }

                newStage.setMaximized(maximized);
                newStage.setTitle(PropertiesService.getInstance().getProperty("title") + " " + PropertiesService.getInstance().getProperty("version"));

                newStage.show();
            }
        });
    }

    /**
     * Get the url of the contents of the relative url
     *
     * @param relativeUrl relative url that is a directory
     * @return contents of the directory
     */
    public List<String> getDirectoryContents(String relativeUrl) {

        List<String> list = new ArrayList<>();

        try {
            URI uri = UtilService.class.getResource(relativeUrl).toURI();
            Path myPath;
            if (uri.getScheme().equals("jar")) {
                if (fileSystem == null) {
                    fileSystem = FileSystems.newFileSystem(uri, Collections.emptyMap());
                }
                myPath = fileSystem.getPath(relativeUrl);

                try (Stream<Path> walk = Files.walk(myPath, 1)) {
                    for (Iterator<Path> it = walk.iterator(); it.hasNext(); ) {
                        list.add(it.next().toString());
                    }
                }
                list.remove(0);
            } else {
                File directory = new File(UtilService.class.getResource(relativeUrl).getPath());

                for (String url : Objects.requireNonNull(directory.list())) {
                    list.add(relativeUrl.replace("/", "\\") + "\\" + url);
                }
            }


        } catch (IOException | URISyntaxException e) {
            Logger.getLogger(UtilService.class.getName()).log(Level.WARNING, e.getMessage());
        }


        return list;
    }

    Object getActiveController() {
        return activeController;
    }
}
