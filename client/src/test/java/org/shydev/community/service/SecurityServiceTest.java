package org.shydev.community.service;

import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.*;

public class SecurityServiceTest {

    private final SecurityService securityService = SecurityService.getInstance();

    @Test
    public void getInstance() {
        // Assert
        assertNotNull(securityService);
    }

    @Test
    public void successfulHash() throws NoSuchAlgorithmException {
        // Arrange
        final String PASSWORD = "SuperSecretSauce420";
        final String ALGORITHM = "SHA-256";

        // Act
        final String RESULT = securityService.hash(PASSWORD, ALGORITHM);

        // Assert
        assertNotNull(RESULT);
        assertEquals(64, RESULT.length());

    }

    @Test
    public void illegalArgumentHash() throws NoSuchAlgorithmException {
        // Arrange
        final String ALGORITHM = "SHA-256";

        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> securityService.hash(null, ALGORITHM));
    }

    @Test
    public void noAlgorithmHash() throws NoSuchAlgorithmException {
        // Arrange
        final String PASSWORD = "SuperSecretSauce420";
        final String ALGORITHM = "SHA-2048";

        // Act & Assert
        assertThrows(NoSuchAlgorithmException.class, () -> securityService.hash(PASSWORD, ALGORITHM));
    }
}