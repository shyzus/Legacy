CREATE DATABASE IF NOT EXISTS `legacy` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `legacy`;
-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: legacy
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
SET NAMES utf8;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `discord_user`
--

DROP TABLE IF EXISTS `discord_user`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
SET character_set_client = utf8;
CREATE TABLE `discord_user`
(
  `discord_id`   int(11)     NOT NULL,
  `discord_name` varchar(45) NOT NULL,
  PRIMARY KEY (`discord_id`),
  UNIQUE KEY `discord_name_UNIQUE` (`discord_name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discord_user`
--

LOCK TABLES `discord_user` WRITE;
/*!40000 ALTER TABLE `discord_user`
  DISABLE KEYS */;
/*!40000 ALTER TABLE `discord_user`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player`
--

DROP TABLE IF EXISTS `player`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
SET character_set_client = utf8;
CREATE TABLE `player`
(
  `player_id`        int(11)     NOT NULL,
  `username`         varchar(45) NOT NULL,
  `create_time`      timestamp   NULL DEFAULT CURRENT_TIMESTAMP,
  `user_user_id`     int(11)     NOT NULL,
  `parent_player_id` int(11)          DEFAULT NULL,
  `parent_user_id`   int(11)          DEFAULT NULL,
  PRIMARY KEY (`player_id`, `user_user_id`),
  KEY `fk_player_user_idx` (`user_user_id`),
  KEY `fk_player_player1_idx` (`parent_player_id`, `parent_user_id`),
  CONSTRAINT `fk_player_player1` FOREIGN KEY (`parent_player_id`, `parent_user_id`) REFERENCES `player` (`player_id`, `user_user_id`),
  CONSTRAINT `fk_player_user` FOREIGN KEY (`user_user_id`) REFERENCES `user` (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player`
--

LOCK TABLES `player` WRITE;
/*!40000 ALTER TABLE `player`
  DISABLE KEYS */;
/*!40000 ALTER TABLE `player`
  ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client = @@character_set_client */;
/*!50003 SET @saved_cs_results = @@character_set_results */;
/*!50003 SET @saved_col_connection = @@collation_connection */;
/*!50003 SET character_set_client = utf8 */;
/*!50003 SET character_set_results = utf8 */;
/*!50003 SET collation_connection = utf8_unicode_ci */;
/*!50003 SET @saved_sql_mode = @@sql_mode */;
/*!50003 SET sql_mode = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */;
DELIMITER ;;
/*!50003 CREATE */ /*!50017 DEFINER =`legacy`@`localhost`*/ /*!50003 TRIGGER `player_BEFORE_INSERT`
  BEFORE INSERT
  ON `player`
  FOR EACH ROW
BEGIN
  SET NEW.create_time = current_timestamp();
END */;;
DELIMITER ;
/*!50003 SET sql_mode = @saved_sql_mode */;
/*!50003 SET character_set_client = @saved_cs_client */;
/*!50003 SET character_set_results = @saved_cs_results */;
/*!50003 SET collation_connection = @saved_col_connection */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
SET character_set_client = utf8;
CREATE TABLE `user`
(
  `user_id`     int(11)      NOT NULL AUTO_INCREMENT,
  `username`    varchar(45)  NOT NULL,
  `email`       varchar(255) NOT NULL,
  `password`    varchar(128) NOT NULL,
  `create_time` timestamp    NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user`
  DISABLE KEYS */;
INSERT INTO `user`
VALUES (2, 'test', 'test@test.nl', '9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08',
        '2018-09-26 12:08:50'),
       (3, 'shyzus', 'shyzus@shyzus.com', '4FADB51C25BC50A79FB3A064752F4B303D1C46A6017F1CDC75D0DE93DB40E33F',
        '2018-12-16 01:22:03');
/*!40000 ALTER TABLE `user`
  ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client = @@character_set_client */;
/*!50003 SET @saved_cs_results = @@character_set_results */;
/*!50003 SET @saved_col_connection = @@collation_connection */;
/*!50003 SET character_set_client = utf8 */;
/*!50003 SET character_set_results = utf8 */;
/*!50003 SET collation_connection = utf8_unicode_ci */;
/*!50003 SET @saved_sql_mode = @@sql_mode */;
/*!50003 SET sql_mode = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */;
DELIMITER ;;
/*!50003 CREATE */ /*!50017 DEFINER =`legacy`@`localhost`*/ /*!50003 TRIGGER `user_BEFORE_INSERT`
  BEFORE INSERT
  ON `user`
  FOR EACH ROW
BEGIN
  SET NEW.create_time = current_timestamp();
END */;;
DELIMITER ;
/*!50003 SET sql_mode = @saved_sql_mode */;
/*!50003 SET character_set_client = @saved_cs_client */;
/*!50003 SET character_set_results = @saved_cs_results */;
/*!50003 SET collation_connection = @saved_col_connection */;

--
-- Table structure for table `warning`
--

DROP TABLE IF EXISTS `warning`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
SET character_set_client = utf8;
CREATE TABLE `warning`
(
  `warning_id`              int(11)   NOT NULL AUTO_INCREMENT,
  `discord_user_discord_id` int(11)        DEFAULT NULL,
  `user_user_id`            int(11)        DEFAULT NULL,
  `create_time`             timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `end_time`                timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`warning_id`),
  KEY `fk_warning_discord_user1_idx` (`discord_user_discord_id`),
  KEY `fk_warning_user1_idx` (`user_user_id`),
  CONSTRAINT `fk_warning_discord_user1` FOREIGN KEY (`discord_user_discord_id`) REFERENCES `discord_user` (`discord_id`),
  CONSTRAINT `fk_warning_user1` FOREIGN KEY (`user_user_id`) REFERENCES `user` (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warning`
--

LOCK TABLES `warning` WRITE;
/*!40000 ALTER TABLE `warning`
  DISABLE KEYS */;
/*!40000 ALTER TABLE `warning`
  ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'legacy'
--

--
-- Dumping routines for database 'legacy'
--
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2019-01-17 19:05:42
