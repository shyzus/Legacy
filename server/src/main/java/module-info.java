module org.shydev.community.server {

    requires java.base;
    requires java.logging;
    requires NetCom2;
    requires java.sql;
    requires java.naming;
    requires net.bytebuddy;
    requires javafx.graphics;
    requires engenius;
    requires org.hibernate.orm.core;
    requires java.persistence;
    requires org.mariadb.jdbc;
    requires java.xml.bind;
    requires com.sun.tools.xjc;
    requires com.sun.xml.bind;
    requires org.hibernate.commons.annotations;

    exports org.shydev.community.model.entity;
    exports org.shydev.community.model;

    opens org.shydev.community.model;
    opens org.shydev.community.model.entity;
    opens org.shydev.community.service;
}