package org.shydev.community;

import com.github.thorbenkuck.netcom2.exceptions.StartFailedException;
import com.github.thorbenkuck.netcom2.network.server.Distributor;
import com.github.thorbenkuck.netcom2.network.shared.CommunicationRegistration;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import nl.hva.fdmci.tsse.engine.service.network.NetworkService;
import org.shydev.community.model.Character;
import org.shydev.community.model.Direction;
import org.shydev.community.model.request.Request;
import org.shydev.community.model.request.RequestType;
import org.shydev.community.model.request.RequestVariable;
import org.shydev.community.service.HibernateService;
import org.shydev.community.service.SaveService;
import org.shydev.community.service.UserService;

import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ServerDriver {

    private static ObservableMap<String, Character> playerMap = FXCollections.observableMap(new ConcurrentHashMap<>());
    private static EnumMap<RequestVariable, Object> contentMap = new EnumMap<>(RequestVariable.class);
    private static final NetworkService NETWORK_INSTANCE = new NetworkService();
    private static Distributor distributor;

    public static void main(String[] args) {
        HibernateService.init();
        try {
            // launch the Server, connecting him to the specified port
            NETWORK_INSTANCE.launchServer(65432);
            // And tell the Server, to always send back the Command, he received
            register(NETWORK_INSTANCE.getServerCommunicationRegistration());
            distributor = Distributor.open(NETWORK_INSTANCE.getServerInstance());
        } catch (StartFailedException e) {
            Logger.getLogger(ServerDriver.class.getName()).log(Level.SEVERE, e.getMessage());
            System.exit(1);
        }
    }

    private static void register(CommunicationRegistration communicationRegistration) {
        communicationRegistration.register(Request.class).addLast((connectionContext, session, request) -> {
            switch (request.getRequestType()) {
                case MOVE:
                    Character modified = playerMap.get(request.getContentMap().get(RequestVariable.USERNAME));
                    modified.setMoving((boolean) request.getContentMap().get(RequestVariable.IS_MOVING));
                    playerMap.replace((String) request.getContentMap().get(RequestVariable.USERNAME), modified);
                    distributor.toAll(request);
                    break;
                case DIRECTION:
                    Character modifiedPlayer = playerMap.get(request.getContentMap().get(RequestVariable.USERNAME));
                    modifiedPlayer.setDirection((Direction) request.getContentMap().get(RequestVariable.DIRECTION));
                    playerMap.replace((String) request.getContentMap().get(RequestVariable.USERNAME), modifiedPlayer);
                    distributor.toAll(request);
                    break;
                case ADD_CHARACTER:
                    Character character = ((Character) request.getContentMap().get(RequestVariable.CHARACTER));
                    character.initializeProperties();
                    playerMap.put(character.getName(), character);

                    contentMap.put(RequestVariable.CHARACTER, character);

                    distributor.toAll(new Request(RequestType.ADD_CHARACTER, contentMap));

                    contentMap.clear();

                    contentMap.put(RequestVariable.PLAYER_MAP, playerMap
                            .entrySet()
                            .stream()
                            .collect(Collectors.toConcurrentMap(Map.Entry::getKey, Map.Entry::getValue)));

                    distributor.toAll(new Request(RequestType.SYNC, contentMap));
                    contentMap.clear();

                    Logger.getLogger(ServerDriver.class.getName()).log(Level.INFO, "Sent sync request");
                    break;
                case SAVE_CHARACTER:
                    SaveService.saveCharacter((Character) request.getContentMap().get(RequestVariable.CHARACTER));
                    break;

                case LOGIN:

                    if (UserService.verifyUserLogin((String) request.getContentMap().get(RequestVariable.USERNAME), (String) request.getContentMap().get(RequestVariable.PASSWORD))) {
                        Character readCharacter = SaveService.readCharacter((String) request.getContentMap().get(RequestVariable.USERNAME));
                        if (readCharacter != null) {

                            contentMap.put(RequestVariable.CHARACTER, readCharacter);
                            contentMap.put(RequestVariable.LOGIN_CONFIRM, true);

                            session.send(new Request(RequestType.LOGIN, contentMap));
                            contentMap.clear();
                            Logger.getLogger(ServerDriver.class.getName()).log(Level.INFO, "Save found for: " +
                                    request.getContentMap().get(RequestVariable.USERNAME));
                        } else {
                            contentMap.put(RequestVariable.USERNAME, request.getContentMap().get(RequestVariable.USERNAME));
                            contentMap.put(RequestVariable.LOGIN_CONFIRM, true);
                            session.send(new Request(RequestType.LOGIN, contentMap));
                            contentMap.clear();
                        }
                        Logger.getLogger(ServerDriver.class.getName()).log(Level.INFO,
                                "Login successful for user: " + request.getContentMap().get(RequestVariable.USERNAME));
                    } else {
                        contentMap.put(RequestVariable.LOGIN_CONFIRM, false);
                        session.send(new Request(RequestType.LOGIN, contentMap));
                        Logger.getLogger(ServerDriver.class.getName()).log(Level.INFO,
                                "Login unsuccessful for user: " + request.getContentMap().get(RequestVariable.USERNAME));
                    }

                    break;
                case LOGOUT:
                    SaveService.saveCharacter(playerMap.get(request.getContentMap().get(RequestVariable.USERNAME)));
                    playerMap.remove(playerMap.get(request.getContentMap().get(RequestVariable.USERNAME)));

                    contentMap.put(RequestVariable.PLAYER_MAP, playerMap
                            .entrySet()
                            .stream()
                            .collect(Collectors.toConcurrentMap(Map.Entry::getKey, Map.Entry::getValue)));

                    distributor.toAll(new Request(RequestType.SYNC, contentMap));
                    contentMap.clear();

                    break;

                case REGISTRATION:
                    if (UserService.verifyUserRegistration((String) request.getContentMap().get(RequestVariable.USERNAME),
                            (String) request.getContentMap().get(RequestVariable.PASSWORD),
                            (String) request.getContentMap().get(RequestVariable.EMAIL))) {
                        contentMap.put(RequestVariable.REGISTRATION_CONFIRM, true);
                        session.send(new Request(RequestType.REGISTRATION, contentMap));
                        contentMap.clear();
                        Logger.getLogger(ServerDriver.class.getName()).log(Level.INFO,
                                "Registration successful for user: " + request.getContentMap().get(RequestVariable.USERNAME));
                    } else {
                        contentMap.put(RequestVariable.REGISTRATION_CONFIRM, false);
                        session.send(new Request(RequestType.REGISTRATION, contentMap));
                        Logger.getLogger(ServerDriver.class.getName()).log(Level.INFO,
                                "Registration unsuccessful for user: " + request.getContentMap().get(RequestVariable.USERNAME));
                        contentMap.clear();
                    }
                    break;

                default:
                    Logger.getLogger(ServerDriver.class.getName()).log(Level.WARNING, "Unsupported request type received.");
            }
        });

    }

}

