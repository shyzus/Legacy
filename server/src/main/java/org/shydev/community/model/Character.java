package org.shydev.community.model;


import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.io.Serializable;

public class Character extends Sprite implements Serializable {

    private String name;
    private boolean moving = false;
    private Direction direction = Direction.SOUTH;
    private String skin;
    private transient SimpleObjectProperty<Direction> directionProperty;
    private transient SimpleBooleanProperty movingProperty;

    public Character(String url, String name, String skin) {
        super(url);
        this.name = name;
        if (skin == null || skin.isBlank()) {
            this.skin = "white";
        } else {
            this.skin = skin;
        }
    }

    public String getSkin() {
        return skin;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
        directionProperty.set(direction);
    }

    public boolean isMoving() {
        return moving;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
        movingProperty.set(moving);
    }

    public SimpleObjectProperty<Direction> getDirectionProperty() {
        return directionProperty;
    }

    public SimpleBooleanProperty getMovingProperty() {
        return movingProperty;
    }

    public void initializeProperties() {
        directionProperty = new SimpleObjectProperty<>(direction);
        movingProperty = new SimpleBooleanProperty(moving);
    }

}
