package org.shydev.community.model;

public enum Direction {

    NORTH("north"), WEST("west"), SOUTH("south"), EAST("east");

    private String value;

    Direction(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
