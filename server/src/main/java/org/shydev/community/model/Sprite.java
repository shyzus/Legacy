package org.shydev.community.model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.Serializable;

public class Sprite extends ImageView implements Serializable {

    private String url;

    public Sprite(String url) {
        super(new Image(Sprite.class.getResourceAsStream(url)));
        this.url = url;
    }

    public Sprite() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        url = url.replace("\\", "/");
        this.url = url;
    }

}
