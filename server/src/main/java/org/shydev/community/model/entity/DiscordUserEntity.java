package org.shydev.community.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "discord_user", schema = "legacy")
public class DiscordUserEntity implements Serializable {
    private int discordId;
    private String discordName;

    @Id
    @Column(name = "discord_id")
    public int getDiscordId() {
        return discordId;
    }

    public void setDiscordId(int discordId) {
        this.discordId = discordId;
    }

    @Basic
    @Column(name = "discord_name")
    public String getDiscordName() {
        return discordName;
    }

    public void setDiscordName(String discordName) {
        this.discordName = discordName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DiscordUserEntity that = (DiscordUserEntity) o;
        return discordId == that.discordId &&
                Objects.equals(discordName, that.discordName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(discordId, discordName);
    }
}
