package org.shydev.community.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "player", schema = "legacy")
@IdClass(PlayerEntityPK.class)
public class PlayerEntity implements Serializable {
    private int playerId;
    private String username;
    private Timestamp createTime;
    private int userUserId;
    private Integer parentPlayerId;
    private Integer parentUserId;

    @Id
    @Column(name = "player_id")
    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Id
    @Column(name = "user_user_id")
    public int getUserUserId() {
        return userUserId;
    }

    public void setUserUserId(int userUserId) {
        this.userUserId = userUserId;
    }

    @Basic
    @Column(name = "parent_player_id")
    public Integer getParentPlayerId() {
        return parentPlayerId;
    }

    public void setParentPlayerId(Integer parentPlayerId) {
        this.parentPlayerId = parentPlayerId;
    }

    @Basic
    @Column(name = "parent_user_id")
    public Integer getParentUserId() {
        return parentUserId;
    }

    public void setParentUserId(Integer parentUserId) {
        this.parentUserId = parentUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayerEntity that = (PlayerEntity) o;
        return playerId == that.playerId &&
                userUserId == that.userUserId &&
                Objects.equals(username, that.username) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(parentPlayerId, that.parentPlayerId) &&
                Objects.equals(parentUserId, that.parentUserId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerId, username, createTime, userUserId, parentPlayerId, parentUserId);
    }
}
