package org.shydev.community.model.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class PlayerEntityPK implements Serializable {
    private int playerId;
    private int userUserId;

    @Column(name = "player_id")
    @Id
    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    @Column(name = "user_user_id")
    @Id
    public int getUserUserId() {
        return userUserId;
    }

    public void setUserUserId(int userUserId) {
        this.userUserId = userUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayerEntityPK that = (PlayerEntityPK) o;
        return playerId == that.playerId &&
                userUserId == that.userUserId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerId, userUserId);
    }
}
