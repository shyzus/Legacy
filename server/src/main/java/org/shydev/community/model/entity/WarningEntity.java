package org.shydev.community.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "warning", schema = "legacy")
public class WarningEntity implements Serializable {
    private int warningId;
    private Integer discordUserDiscordId;
    private Integer userUserId;
    private Timestamp createTime;
    private Timestamp endTime;

    @Id
    @Column(name = "warning_id")
    public int getWarningId() {
        return warningId;
    }

    public void setWarningId(int warningId) {
        this.warningId = warningId;
    }

    @Basic
    @Column(name = "discord_user_discord_id")
    public Integer getDiscordUserDiscordId() {
        return discordUserDiscordId;
    }

    public void setDiscordUserDiscordId(Integer discordUserDiscordId) {
        this.discordUserDiscordId = discordUserDiscordId;
    }

    @Basic
    @Column(name = "user_user_id")
    public Integer getUserUserId() {
        return userUserId;
    }

    public void setUserUserId(Integer userUserId) {
        this.userUserId = userUserId;
    }

    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "end_time")
    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WarningEntity that = (WarningEntity) o;
        return warningId == that.warningId &&
                Objects.equals(discordUserDiscordId, that.discordUserDiscordId) &&
                Objects.equals(userUserId, that.userUserId) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(endTime, that.endTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(warningId, discordUserDiscordId, userUserId, createTime, endTime);
    }
}
