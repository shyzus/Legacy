package org.shydev.community.model.request;

import java.io.Serializable;

public enum RequestType implements Serializable {
    LOGIN, REGISTRATION, SAVE_CHARACTER, ADD_CHARACTER, REMOVE_CHARACTER, LOGOUT, MOVE, DIRECTION, SYNC
}
