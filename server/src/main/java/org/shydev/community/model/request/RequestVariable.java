package org.shydev.community.model.request;

import java.io.Serializable;

public enum RequestVariable implements Serializable {
    USERNAME, IS_MOVING, DIRECTION, PASSWORD, EMAIL, CHARACTER, PLAYER_MAP, LOGIN_CONFIRM, REGISTRATION_CONFIRM,
}
