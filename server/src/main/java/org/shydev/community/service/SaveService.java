package org.shydev.community.service;

import org.shydev.community.model.Character;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Save service responsible for saving character save files and reading them
 *
 * @author shyzus (dev@shyzus.com)
 */
public class SaveService {

    private static final String LEGACY_DIRECTORY = System.getProperty("user.home").concat("\\.legacy");

    private SaveService() {
    }

    private static Logger saveServiceLogger = Logger.getLogger(SaveService.class.getName());

    public static void saveCharacter(Character character) {

        if (!Files.isDirectory(Paths.get(LEGACY_DIRECTORY))) {
            try {
                Files.createDirectory(Paths.get(LEGACY_DIRECTORY));
            } catch (IOException e) {
                saveServiceLogger.log(Level.WARNING, e.getMessage());
            }
        }

        try (
                FileOutputStream f = new FileOutputStream(new File(LEGACY_DIRECTORY
                        .concat(character.getName())
                        .concat(".save")));
                ObjectOutputStream o = new ObjectOutputStream(f)
        ) {

            // Write objects to file
            o.writeObject(character);

            saveServiceLogger.log(Level.INFO, "Saved character: " + character.getName());
        } catch (IOException e) {
            saveServiceLogger.log(Level.WARNING, "Error initializing stream.");
        }

    }


    public static Character readCharacter(String name) {

        Character character = null;

        if (!Files.isDirectory(Paths.get(System.getProperty("user.home")))) {
            saveServiceLogger.log(Level.WARNING, "Directory not found.");
            return null;
        }

        try (
                FileInputStream fi = new FileInputStream(LEGACY_DIRECTORY.concat(name).concat(".save"));
                ObjectInputStream oi = new ObjectInputStream(fi)
        ) {

            // Read objects
            character = (Character) oi.readObject();

            saveServiceLogger.log(Level.INFO, "Read character: " + character.getName());

            return character;

        } catch (IOException e) {
            saveServiceLogger.log(Level.WARNING, "Error initializing stream.");
        } catch (ClassNotFoundException e) {
            saveServiceLogger.log(Level.SEVERE, "Class cast failure.");
        }

        return character;
    }
}
