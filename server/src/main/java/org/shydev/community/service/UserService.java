package org.shydev.community.service;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.shydev.community.model.entity.UserEntity;

import javax.persistence.NoResultException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class UserService {

    private UserService() {
    }

    public static boolean verifyUserLogin(String username, String password) {

        Session session = HibernateService.getSession();

        Query query = session.getNamedQuery("findUserByUsername")
                .setParameter("username", username);

        Object result = null;

        try {
            result = query.getSingleResult();
            if (result != null) {
                UserEntity userEntity = (UserEntity) result;
                session.close();
                return password.equals(userEntity.getPassword());
            }
        } catch (NoResultException e) {
            Logger.getLogger(UserService.class.getName()).log(Level.INFO, e.getMessage());
        }

        session.close();
        return false;
    }

    public static boolean verifyUserRegistration(String username, String password, String email) {
        UserEntity userEntity = new UserEntity();

        userEntity.setEmail(email);
        userEntity.setUsername(username);
        userEntity.setPassword(password);
        Session session = HibernateService.getSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.save(userEntity);
            transaction.commit();
            session.close();
            return true;
        } catch (HibernateException e) {
            Logger.getLogger(UserService.class.getName()).log(Level.INFO, e.getMessage());
        }
        transaction.commit();
        session.close();
        return false;
    }

}
