package org.shydev.community.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.shydev.community.model.Character;

public class SaveServiceTest {

    private Character character;

    @BeforeEach
    public void setUp() throws Exception {
        // Arrange
        character = new Character("/assets/base/white/south/standing/0.png", "testUser", null);
    }

    @AfterEach
    public void tearDown() throws Exception {
        character = null;
    }

    @Test
    public void saveCharacter() {
        // Act
        SaveService.saveCharacter(character);

        // Assert
        Assertions.assertEquals(character.getName(), SaveService.readCharacter("testUser").getName());
    }

    @Test
    public void readCharacter() {
    }
}